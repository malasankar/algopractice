package main.java.org.msankar.examples;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class DocDistance {
	public static void main(String[] args) {
		DocDistance docDist = new DocDistance();
		// 1. Build a map of words in each doc to the number of times it occurs.
		Map<String, Integer> map1 = docDist.getMapOfWordsToNumOfOccurences("/Users/malathi.sankar/algoLearning/algo/src/main/resources/file1.txt");
		Map<String, Integer> map2 = docDist.getMapOfWordsToNumOfOccurences("/Users/malathi.sankar/algoLearning/algo/src/main/resources/file2.txt");
		
		// 2. Compare the lengths of the two maps. Disregard the sequence of the words.
		int wordNumInDoc1 = 0, wordNumInDoc2 = 0 ;
		double sumOfWordProduct = 0;
		double sumOfWordNumInDoc1Sq = 0 , sumOfWordNumInDoc2Sq = 0;
		
		for (String word : map1.keySet()) {
			if (map2.containsKey(word)) {
				wordNumInDoc1 = map1.get(word);
				wordNumInDoc2 = map2.get(word);
				
				sumOfWordProduct += wordNumInDoc1 * wordNumInDoc2;
				sumOfWordNumInDoc1Sq += wordNumInDoc1*wordNumInDoc1;
				sumOfWordNumInDoc2Sq += wordNumInDoc2*wordNumInDoc2;
			}
		}
		
		double result = Math.acos( sumOfWordProduct / 
				(Math.sqrt(sumOfWordNumInDoc1Sq) * Math.sqrt(sumOfWordNumInDoc2Sq)));		
		
		if (result < 0.001) {
			System.out.println("These docs are identical " + result);			
		} else {
			System.out.println("These docs are NOT identical " + result);
		}
	}
	
	public Map<String, Integer> getMapOfWordsToNumOfOccurences(String filePath) {
		Map<String, Integer> retMap = new HashMap<> ();
		
		try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
			String line = "";			
			while ((line = br.readLine()) != null) {
				String[] wordList = line.split(" ");
				String word;
				int occurs = 0;
				
				for (int i = 0; i < wordList.length; i++) {
					word = wordList[i].toLowerCase().trim().replaceAll("\\p{Punct}", "");
					occurs = retMap.get(word)==null ? 0 : retMap.get(word);
					retMap.put(word, occurs + 1);
				}
				//System.out.println(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return retMap;
	}
}

